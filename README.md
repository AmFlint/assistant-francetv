# Equipe 16 - France TV Prototype


## Concept

Nous souhaitons développer la convivialité/l'entrain communautaire autour d'évènements sportifs proposés par France TV.

Pour se faire nous souhaitons permettre aux téléspectateurs de réagir avec la communauté sans quitter l'écran des yeux. Bien entendu, ce procédé ne réinvente pas l'expérience des spectateurs, mais l'enrichit. Le sport se vit pleinement, et il est important de donner aux spectateurs l'occasion d'exprimer leurs émotions dans les meilleures conditions.

Afin d'amener les utilisateurs à consommer les fonctionnalités `pendant l'évènement`, nous proposons également différentes fonctionnalités accessibles à tout moment. Nous parlons ici de fonctionnalités basiques comme par exemple suivre le planning des directs, accéder aux dernières actualités rapidement, partager des temps forts, configurer des rappels pour des directs ou encore lancer le direct.

Notre solution est disposée sur plusieurs parties:
- **Hors évènement sportif**:
  - Consulter les dernières actualités
  - Consulter et partager les temps forts d'un précédent évènement
  - Consulter le planning des directs
  - Dans le cas où un direct est en cours, possibilité de suivre/lancer le direct
- **Pendant l'évènement sportif**: 
  - Suivre les réactions de la communauté (en temps réel)
  - Réagir à des actions (partager un temps fort: un but par exemple) ou/et des réactions de la communauté.

## Les technologies utilisées

Pour développer ce Prototype, nous nous sommes basés sur les technologies suivantes:
- Google Assistant
- Google Cloud Functions (=> `Firebase`)
- node.js v8 (avec `TypeScript`)

Souhaitant proposer une solution clé-en-main, dés à présent intégrée avec la partie existante, nous avons utilisé le concept de `Scraping Web` (parcourir des pages Web depuis un script pour récupérer des informations spécifiques).
Cela nous permet d'hydrater notre assistant directement depuis les données présentées par France TV sur leur [site sport](https://sport.francetvinfo.fr).

La technologie utilisée s'appelle [JSDOM](https://github.com/jsdom/jsdom), cette librairie permet d'exploiter le code source d'une page Web, et met à la disposition des développeurs un objet `window` similaire à celui d'un navigateur (donne donc l'opportunité d'utiliser des `document.querySelector` pour extraire des informations particulières).

## Architecture du projet

Le dossier **functions** contient toute la logique `serveur` exécutée lors d'une question posée à l'Assistant Google

À l'intérieur de ce répertoire, on peut retrouver l'arborescence suivante:
- `lib`: dossier crée lors de la compilation des fichiers TypeScript vers les fichiers finaux en JavaScript executés dans le Cloud Google.
- `src`: (contenu du projet)
  - `intents`: (dossier) contient la logique pour répondre aux intents
    - `get_news.ts`: Contient le code pour l'intent `voir les actualités` à savoir appeler le helper de scraping, et formatter la réponse pour le Google Assistant.
    - `search_directs.ts`: Contient le code pour l'intent `suivre les directs` à savoir appeler le helper de scraping et formatter la réponse.
    - `start_direct.ts`: Intent `lancer le direct`
  - `models`: (dossier): Contient les structures de données utilisées par l'application
  - `scraper` (dossier): Logique pour le scraping de France TV.
    - `news.ts`: Contient la logique de scraping afin de récupérer les informations (titre, description, image, lien) vers les dernières news.
    - `directs.ts`: Contient la logique de scraping afin de récupérer les informations (titre, date, image) des directs à venir, et du direct en cours si il y en a un.
  - `index.ts`: point d'entrée de la fonction, contient
  - `constants.ts`: Contient les constantes comme les `URL` nécessaires pour scraper France TV
- `package.json` et `package-lock.json`: dépendances nécessaires au projet pour fonctionner
- `fichiers firebase` pour la configuration du projet.

Nous proposons dans ce prototype fonctionnel 3 intents:
- `voir les actualités`: Permet de voir une liste d'actualités récupérées depuis le site sports de France TV.
- `voir les directs à venir`: voir la programmation des directs à venir sur la chaîne, informe également si un direct est en cours
- `lancer le direct`: Lance un évènement en direct si il en existe un en cours, sinon affiche la liste des évènements à venir.
