import { scrapeDirects } from '../scraper/directs';
import { SimpleResponse, BrowseCarousel, BrowseCarouselItem, Image, Suggestions } from 'actions-on-google';
import { DIRECT_URL } from '../constants';

const searchDirects = async (conv: any) => {
  const currentDirects = await scrapeDirects();

  let directsSpeech = 'Aucun évènement n\'est en cours, voici les évènements sportifs à venir sur le direct';
  const directsTxt = 'Voici les évènements sportifs à venir en direct'
  if (currentDirects.isDirectHappening) {
    directsSpeech = `Un évènement est en cours sur le direct: ${currentDirects.currentDirectTitle}, à suivre sur France TV`;

    conv.ask(new Suggestions(['lancer le direct', 'voir les actualités']));
  }

  const nbDirects = currentDirects.directs.length;
  const nextDirects = currentDirects.directs.slice(0, nbDirects > 10 ? 10 : nbDirects);

  conv.ask(new SimpleResponse({
    speech: directsSpeech,
    text: directsTxt,
  }));

  conv.ask(new BrowseCarousel({
    items: nextDirects.map(({ title, description, image }) => new BrowseCarouselItem({
      title,
      description,
      url: DIRECT_URL,
      image: new Image({
        url: image,
        alt: 'direct a venir'
      })
    })),
  }));
}

export default searchDirects;
