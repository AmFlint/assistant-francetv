import { Image, SimpleResponse, BrowseCarousel, BrowseCarouselItem, Suggestions } from 'actions-on-google';
import Article from '../models/Article';
import { scrapeNews } from '../scraper/news';

const getNews = async (conv: any) => {
  const articles = await scrapeNews();
  const carouselItems = articles.map(({ title, category, image, url }: Article) => new BrowseCarouselItem({
    title,
    url,
    description: category,
    image: new Image({
      url: image,
      alt: 'image'
    })
  }));

  conv.ask(new SimpleResponse({
    text: 'Voici les dernières actualités sur France tv sports',
    speech: 'Voici les dernières actualités sur France tv sports',
  }))

  conv.ask(new Suggestions(['directs à venir', 'lancer le direct']));

  conv.ask(new BrowseCarousel({
    items: carouselItems,
  }));
}

export default getNews;
