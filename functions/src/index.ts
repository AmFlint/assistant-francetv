import * as functions from 'firebase-functions';
import { dialogflow } from 'actions-on-google';
import getNews from './intents/get_news';
import searchDirects from './intents/search_directs';
import startDirect from './intents/start_direct';

const app = dialogflow({ debug: true });

// News
app.intent('get_news', getNews);
// Directs
app.intent('search_directs', searchDirects);
app.intent('start_direct', startDirect);

export const fullfillments = functions.https.onRequest(app);
