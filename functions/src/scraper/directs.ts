import { JSDOM } from 'jsdom';
import Direct from '../models/Direct';
import { DIRECT_URL } from '../constants';

const MONTHS = {
  'jui.': 6,
  'sep.': 8,
}

const MONTHS_FR = [
  'janvier',
  'février',
  'mars',
  'avril',
  'mai',
  'juin',
  'juillet',
  'aout',
  'septembre',
  'octobre',
  'novembre',
  'décembre'
];

export const scrapeDirects = async () => {
  const { window: { document } } = await JSDOM.fromURL(DIRECT_URL, { runScripts: 'outside-only' });

  const directElements = document.querySelectorAll('.live-item');

  let lastDateFound = new Date();

  const directs: Direct[] = [];
  directElements.forEach((directElement) => {
    // Manage events's date/planning
    lastDateFound = getDateFromDirect(directElement, lastDateFound);
    // 
    const titleElement = directElement.querySelector('.live-txt');
    const title = titleElement ? (titleElement.textContent || '') : '';

    const startingHourElement = directElement.querySelector('.live-time');
    const startingHour = startingHourElement ? (startingHourElement.textContent || '') : '';
    const splitHourMinutes = startingHour.split('h');

    // Format
    const startingHourFormatted = `${lastDateFound.getDate()} ${MONTHS_FR[lastDateFound.getMonth()]} à ${startingHour}`;
    const imageElement = directElement.querySelector('img');
    const image = imageElement ? imageElement.src : '';

    const date = new Date(
      2018,
      lastDateFound.getMonth(),
      lastDateFound.getDate(),
      parseInt(splitHourMinutes[0]),
      parseInt(splitHourMinutes[1])
    );

    directs.push({
      title,
      image,
      date,
      description: startingHourFormatted,
    })
  })

  // This div only shows up if a direct is happening
  const isDirectElement = document.querySelector('#PlayerContainer');
  const directTitleElement = document.querySelector('.video-page-title');
  const directDescriptionElement = document.querySelector('.page-main-content strong');
  const directDescription = directDescriptionElement ? (directDescriptionElement.textContent || '') : '';
  //@ts-ignore
  const isDirectHappening = isDirectElement ? isDirectElement.dataset.type === 'direct' && isDirectElement.dataset.video !== '' : false;
  return {
    directs,
    isDirectHappening,
    directDescription,
    currentDirectTitle: directTitleElement ? (directTitleElement.textContent || '') : ''
  };
};


const getDateFromDirect = (directElement: Element, lastDateFound: Date) => {
  // Event date might be `null` => exists only for first event of given date, visually, all following elements
  // appear to belong to the same date
  const eventDate = directElement.querySelector('.live-datetime time');
  if (eventDate) {
    const dayOfTheMonthElement = eventDate.querySelector('.live-day-number');
    const dayOfTheMonth = dayOfTheMonthElement ? (dayOfTheMonthElement.textContent || '') : '';
    // Month is in format: 'jui.' for juillet, 'sep.' for september...
    const monthElement = eventDate.querySelector('.live-month');
    const month = monthElement ? (monthElement.textContent || '') : '';

    // Remember last date encountered for event planning
    //@ts-ignore
    lastDateFound.setMonth(MONTHS[month]);
    lastDateFound.setDate(parseInt(dayOfTheMonth));
  }

  return lastDateFound;
}
