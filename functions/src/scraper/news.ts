import { JSDOM } from 'jsdom';
import Article from '../models/Article';
import { NEWS_URL } from '../constants';

/**
 * Scrape France tv's news web page, retrieve last 4 articles.
 */
export const scrapeNews = async (): Promise<Article[]> => {
  const { window: { document } } = await JSDOM.fromURL(NEWS_URL, {});

  const articles = document.querySelectorAll('.flux-card');
  if (!articles || !articles.length) {
    throw new Error('Aucun article trouvé');
  }

  // get first 4 articles
  const firstFewArticles: Element[] = [];
  const nbArticles = articles.length > 10 ? 10 : articles.length;
  for (let i = 0; i < nbArticles; i++) {
    firstFewArticles.push(articles.item(i));
  }

  // Format responses
  return firstFewArticles.map(transformDomIntoArticle);
};

const transformDomIntoArticle = (article: Element) => {
  const title = article.querySelector('h3');
  const category = article.querySelector('.flux-card__theme-title')
  const image = article.querySelector('img');
  const url = article.querySelector('a');

  return {
    title: title ? (title.textContent || '') : '',
    category: category ? (category.textContent || '') : '',
    image: image ? (image.dataset.original || image.src) : '',
    url: url ? url.href : ''
  };
}
