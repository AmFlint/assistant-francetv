type Article = {
  title: string,
  category: string,
  image: string,
  url: string,
}

export default Article;
