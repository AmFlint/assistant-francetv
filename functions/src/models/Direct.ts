type Direct = {
  title: string,
  description: string,
  image: string,
  date: Date,
};

export default Direct;
